![Crawfish](asset/logo-nobackground.png)


**Crawfish** is an innovative and free torrent client where anyone can stream/download torrents and share it with a simple link.


Available feature is:
- Basic torrent feature (Download/Seed)
- Torrent seeded by this client can be streamed or downloaded from webtorrent browser client like QPlayer. Example: [QPlayer with magnet](https://tndsite.gitlab.io/quix-player/?magnet=magnet:?xt=urn:btih:08ada5a7a6183aae1e09d831df6748d566095a10&dn=Sintel&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com&ws=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2F&xs=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2Fsintel.torrent)
- Search torrent direct in the client [Based on **[Searx](https://searx.me/)** service fetch data from **nyaa** and **1337**]
- Possibility to share a link to download file in the browser
- File can be opened even if the download is not finished yet


> How I can access to the service?

Download the latest release here. [Download CrawFish](https://github.com/drakonkat/webtorrent-express-api/releases)

> Is possible to share link a torrent to a friend?

**Yes!** Simply clicking the share element icon ![Share icon](asset/ShareIcon.png) 



It uses **[WebTorrent-hybrid](https://github.com/webtorrent/webtorrent-hybrid)** - the first torrent client that works in the browser. **WebTorrent** uses **[WebRTC](https://webrtc.org/)** for true peer-to-peer transport. No browser plugin, extension, or installation is required.


---

### Note

> There is a way to support this project?

**Yes!** Simply use the software, open issue if there is improvement/bug fixing that can be done and say thanks! It is enough

Issue can be send here. [Issue board](https://github.com/drakonkat/webtorrent-express-api/issues)

## License

MIT. Copyright (c) [Drakonkat](https://gitlab.com/tndsite/quix-player/-/blob/master/LICENSE).
